#pragma once
#include "General_Lib.h"
#include <cmath>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>

//--------------------------------------------------------------------------
class Vehicle
{
public:
	double mLength;
	double mSteeringDelay;
	double mSteeringDumping;
	double mSteeringBandwidth;
	SmartPointer<double> mKinematicState;

	Vehicle() :
		mLength(0),
		mSteeringDelay(ZERO_LIMIT),
		mSteeringDumping(ZERO_LIMIT),
		mSteeringBandwidth(ZERO_LIMIT),
		mKinematicState(7)
	{};
	
	Vehicle(double vLength, double vSteeringDelay = ZERO_LIMIT, double vSteeringDumping = ZERO_LIMIT, double vSteeringBandwidth = ZERO_LIMIT)
	{
		mLength = vLength;
		mSteeringDelay = vSteeringDelay;
		mSteeringDumping = vSteeringDumping;
		mSteeringBandwidth = vSteeringBandwidth;
		mKinematicState.Reset(7);
	}

	void SetVehicleParameters(double vLength = 0, double vSteeringDelay = ZERO_LIMIT, double vSteeringDumping = ZERO_LIMIT, double vSteeringBandwidth = ZERO_LIMIT)
	{
		mLength = vLength;
		mSteeringDelay = vSteeringDelay;
		mSteeringDumping = vSteeringDumping;
		mSteeringBandwidth = vSteeringBandwidth;
	};

	void SetVehicleState(double vX, double vY, double vPsi, double vDelta, double vVel)
	{
		mKinematicState[0] = vX;
		mKinematicState[1] = vY;
		mKinematicState[4] = vPsi;
		mKinematicState[5] = vDelta;
		mKinematicState[6] = vVel;
	};

	void SetVehicleState(const SmartPointer<double>& vKinematicState)
	{
		for (int i = 0; i <= vKinematicState.GetSize(); i++)
			mKinematicState[i] = vKinematicState[i];
	};

	void GetState(SmartPointer<double>& vStateToShow, int vOptionCS = 0)
	{
		vStateToShow.Reset(mKinematicState.GetSize());
		for (int i = 0; i <= mKinematicState.GetSize(); i++)
			vStateToShow[i] = mKinematicState[i];

		if (vOptionCS == 0)		//State in GCS
		{
			vStateToShow[5] += vStateToShow[4];
		}
		else if (vOptionCS == 1)	//State in ECS
		{
			vStateToShow[0] = 0;
			vStateToShow[1] = 0;
			vStateToShow[4] = -vStateToShow[4];
		}
	}

	void GetStateDynamics(SmartPointer<double>& vStateToShow)
	{
		vStateToShow.Reset(3);

		double v = mKinematicState[6];
		double psi = mKinematicState[4];
		double delta = mKinematicState[5];

		vStateToShow[0] = v * cos(psi);
		vStateToShow[1] = v * sin(psi);
		vStateToShow[2] = v * tan(delta) / mLength;

		if (abs(vStateToShow[2]) > 10 * PI)		// Protection against drastic angle change due to delta = pi / 2
			vStateToShow[2] = 10 * PI * Sign(vStateToShow[2]);
	}

	double GetSteeringDynamics(double t, int vOrder = 1)
	{
		// vOrder = 0 --> no dynamics, direct value
		double tmp = 1;
		if (vOrder == 1)
			tmp = (1 - exp(-t / mSteeringDelay));
		else if (vOrder == 2)
		{
			double xii = mSteeringDumping;
			double wn = mSteeringBandwidth;
			double stta = sqrt(1 - xii * xii);
			double wd = wn * stta;
			tmp = (1 - exp(-xii * wn * t) * (cos(wd * t) + xii / stta * sin(wd * t)));
		}

		tmp *= mKinematicState[5];	//Multiply by delta amplitude
		
		return tmp;
	}

	void ProgressVehicleMotion(double t, double dt, double vDelta)
	{
		//First update delta to get dynamics
		mKinematicState[5] = vDelta;

		//Collect states
		double delta = GetSteeringDynamics(t, 1);
		double x = mKinematicState[0];
		double y = mKinematicState[1];
		double psi = mKinematicState[4];

		//Collect state derivatives
		SmartPointer<double> tmp;
		GetStateDynamics(tmp);

		//Update new state
		mKinematicState[0] = x + tmp[0] * dt;
		mKinematicState[1] = y + tmp[1] * dt;
		mKinematicState[4] = psi + tmp[2] * dt;
	}

};