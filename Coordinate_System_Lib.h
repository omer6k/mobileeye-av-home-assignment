#pragma once
#include "General_Lib.h"
#include <cmath>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <random>

//--------------------------------------------------------------------------
typedef struct PathPoint;
class Path;
int Sign(double x);
bool TranslationZRotation(const SmartPointer<PathPoint>& vDataIn, PathPoint oPreRotation, PathPoint oPostRotation, double psi, SmartPointer<PathPoint>& vDataOut);
bool ECS2GCS(const SmartPointer<PathPoint>& vDataInECS, PathPoint o, double psi, SmartPointer<PathPoint>& vDataInGCS);
bool GCS2ECS(const SmartPointer<PathPoint>& vDataInGCS, PathPoint o, double psi, SmartPointer<PathPoint>& vDataInECS);
bool GCS2PCS(const SmartPointer<PathPoint>& vDataInGCS, SmartPointer<PathPoint>& vDataInPCS);

//--------------------------------------------------------------------------
typedef struct PathPoint
{
	double x, y, r;

	PathPoint() :
		x(0),
		y(0),
		r(0)
	{};
	PathPoint(double vX, double vY)
	{
		x = vX;
		y = vY;
		r = 0;
	};
	PathPoint(double vX, double vY, double vR)
	{
		x = vX;
		y = vY;
		r = vR;
	};
	PathPoint(const PathPoint& p)
	{
		x = p.x;
		y = p.y;
		r = p.r;
	};
} PathPoint;

class Path
{
private:
	int mSize;
public:
	SmartPointer<PathPoint> mData;

	Path() : 
		mData(),
		mSize(0)
	{};
	Path(int vSize)
	{
		SetDataSize(vSize);
	};
	Path(const Path& vPath)
	{
		SetDataSize(vPath.mSize);
		CopyData(vPath.mData);
	};
	void SetDataSize(int size)
	{
		mSize = size;
		mData.Reset(size);
	};
	void CopyData(const SmartPointer<PathPoint>& vData)
	{
		mData.Reset(vData.GetSize());
		for (int i = 0; i < vData.GetSize(); i++)
		{
			mData[i].x = vData[i].x;
			mData[i].y = vData[i].y;
			mData[i].r = vData[i].r;
		}
	};
	void CopyData(const Path& vPath)
	{
		mData.Reset(vPath.mData.GetSize());
		for (int i = 0; i < vPath.mData.GetSize(); i++)
		{
			mData[i].x = vPath.mData[i].x;
			mData[i].y = vPath.mData[i].y;
			mData[i].r = vPath.mData[i].r;
		}
	};

	int GetSize()
	{
		return mSize;
	};
	void SetCircleTrajectory(PathPoint vCenter, double vRaduis)
	{
		if (mSize == 0)
		{
			std::cout << "Invalid Data Size" << std::endl;
			return;
		}

		double tta = 0;
		double dtta = 2 * PI / (mSize-1);
		for (int i = 0; i < mSize; i++)
		{
			mData[i].x = vCenter.x + vRaduis * cos(tta);
			mData[i].y = vCenter.y + vRaduis * sin(tta);
			mData[i].r = vRaduis;
			tta += dtta;
		}
	};
	void SetLineTrajectory(PathPoint vStart, PathPoint vEnd)
	{
		if (mSize == 0)
		{
			std::cout << "Invalid Data Size" << std::endl;
			return;
		}

		double dx = (vEnd.x - vStart.x) / mSize;
		double dy = (vEnd.y - vStart.y) / mSize;
		for (int i = 0; i < mSize; i++)
		{
			mData[i].x = vStart.x + dx * i;
			mData[i].y = vStart.y + dy * i;
			mData[i].r = INFINITY;
		}
	};
	void GetPath(Path& vPathToShow, int vOptionCS = 0)
	{
		vPathToShow.mSize = mData.GetSize();

		if (vOptionCS == 0)		//Path in GCS
			vPathToShow.CopyData(mData);
		else if (vOptionCS == 1)	//Path in PCS
			GCS2PCS(mData, vPathToShow.mData);
	};
	void Draw(cv::Mat img, int vHeight = 500, int vWidth = 500, int vScale = 1)
	{
		if ((mSize > 0) && (mData.mData))
		{
			int r0 = 0;
			int r1 = 500;
			int c0 = 0;
			int c1 = 500;
			for (int i = 0; i < mSize; i++)
			{
				int x = static_cast<int>(mData[i].x);
				int y = static_cast<int>(mData[i].y);
				if (y < 0)
					y = 0;
				if (x < 0)
					x = 0;
				if (r0 > y)
					r0 = y;
				if (r1 < y)
					r1 = y;
				if (c0 > x)
					c0 = x;
				if (c1 < x)
					c1 = x;
			}
			int width = (c1 - c0);
			int height = (r1 - r0);
			if (width > 500)
				width = 500;
			if (height > 500)
				height = 500;

			for (int c = 0; c < 3; c++)
			{
				int color = rand() % 255;
				for (int i = 0; i < mSize; i++)
				{
					int x = static_cast<int>((vScale * mData[i].x + width) / 2);
					int y = static_cast<int>((-vScale * mData[i].y + height) / 2);

					if ((y >= 0) && (y < height) && (x >= 0) && (x < width))
						img.at<cv::Vec3b>(y, x)[c] = color;
				}
			}
		}
	};
	void Show(int vScale = 8)
	{
		cv::Mat img = cv::Mat(500, 500, CV_8UC3, cv::Scalar(255, 255, 255));
		Draw(img, 500, 500, vScale);
		cv::imshow("Trajectory In GCS", img);
		cv::waitKey(0);
	}


};

//--------------------------------------------------------------------------
bool TranslationZRotation(const SmartPointer<PathPoint>& vDataIn, PathPoint oPreRotation, PathPoint oPostRotation, double psi, SmartPointer<PathPoint>& vDataOut)
{
	vDataOut.Reset(vDataIn.GetSize());
	
	double sin_psi = sin(psi);
	double cos_psi = cos(psi);

	for (int i = 0; i < vDataIn.GetSize(); i++)
	{
		vDataOut[i].x = oPostRotation.x + cos_psi * (vDataIn[i].x - oPreRotation.x) + sin_psi * (vDataIn[i].y - oPreRotation.y);
		vDataOut[i].y = oPostRotation.y + -sin_psi * (vDataIn[i].x - oPreRotation.x) + cos_psi * (vDataIn[i].y - oPreRotation.y);
	}

	return true;
};

bool ECS2GCS(const SmartPointer<PathPoint>& vDataInECS, PathPoint o, double psi, SmartPointer<PathPoint>& vDataInGCS)
{
	if (TranslationZRotation(vDataInECS, o, PathPoint(0, 0), -psi, vDataInGCS))
		return true;

	return false;
}

bool GCS2ECS(const SmartPointer<PathPoint>& vDataInGCS, PathPoint o, double psi, SmartPointer<PathPoint>& vDataInECS)
{
	if (TranslationZRotation(vDataInGCS, o, PathPoint(0,0), psi, vDataInECS))
		return true;

	return false;
}

bool GCS2PCS(const SmartPointer<PathPoint>& vDataInGCS, SmartPointer<PathPoint>& vDataInPCS)
{
	int N = vDataInGCS.GetSize();

	vDataInPCS.Reset(N);
	double dx = 0, dy = 0, dydx = 0;
	for (int i = 1; i < N; i++)
	{
		dx = vDataInGCS[i].x - vDataInGCS[i - 1].x;
		if (abs(dx) < ZERO_LIMIT)
			dx = ZERO_LIMIT * Sign(dx);
		dy = vDataInGCS[i].y - vDataInGCS[i - 1].y;
		dydx = dy / dx;

		vDataInPCS[i].x += vDataInPCS[i - 1].x + abs(sqrt(1 + dydx * dydx) * dx);
	}
	
	return true;
}

int Sign(double x)
{
	if (x >= 0)
		return 1;
	else if (x < 0)
		return -1;
}

int FindNearestPointPCS(const SmartPointer<PathPoint>& vDataInPCS, PathPoint vPointInPCS)
{
	double vMin = INF;
	int iMin = 0;
	for (int i = 0; i < vDataInPCS.GetSize(); i++)
		if (abs(vDataInPCS[i].x - vPointInPCS.x) < vMin)	//s coordinate in PCS is represented by x coordinate
		{
			iMin = i;
			vMin = abs(vDataInPCS[i].x - vPointInPCS.x);
		}

	return iMin;
}

double CalculatePathCurvature(const SmartPointer<PathPoint>& vData)
{
	double dx = 0, dy = 0;
	double dydx = 0, d2ydx2 = 0;
	double r = 0;
	for (int i = 2; i < vData.GetSize(); i++)
	{
		dx = vData[i].x - vData[i - 1].x;
		if (abs(dx) < ZERO_LIMIT)
			dx = ZERO_LIMIT * Sign(dx);
		dy = vData[i].y - vData[i - 1].y;
		dydx = dy / dx;
		d2ydx2 = (vData[i].y / dx) - (vData[i - 1].y);
		//Need to finish implementation
	}

	return r;
}