#pragma once
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

//--------------------------------------------------------------------------
const double PI = 3.141592653589793;
const double ZERO_LIMIT = 1e-20;
const double INF = 1e100;

//--------------------------------------------------------------------------
template <class T>
class SmartPointer
{
private:
	int mSize;
public:
	T* mData;
	SmartPointer() :
		mData(nullptr),
		mSize(0)
	{};
	SmartPointer(int size)
	{
		Reset(size);
	};
	SmartPointer(int size, T vInitValue)
	{
		Reset(size, vInitValue);
	};
	SmartPointer(T* pArrayToCopy, int size)
	{
		Reset(size);
		for (int i = 0; i < mSize; i++)
			mData[i] = pArrayToCopy[i];
	};
	~SmartPointer()
	{
		if (mData)
			delete(mData);
	};
	void Reset(int size)
	{
		if (mData)
			delete(mData);
		mSize = size;
		mData = (T*) malloc (mSize * sizeof(T));
	}
	void Reset(int size, T vInitValue)
	{
		Reset(size);
		//memset(mData, vInitValue, mSize * sizeof(T));
		for (int i = 0; i < mSize; i++)
			mData[i] = vInitValue;	
	};
	T operator [](int index) const
	{
		if ((index >= 0) && (mSize > 0) && (index < mSize))
			return mData[index];
		return mData[0];
	};
	T& operator[](int index)
	{
		if ((index >= 0) && (mSize > 0) && (index < mSize))
			return mData[index];
		return mData[0];
	};
	int GetSize() const
	{
		return mSize;
	};

};