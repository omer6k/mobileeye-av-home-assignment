#pragma once
#include "General_Lib.h"
#include "Coordinate_System_Lib.h"
#include "Vehicle_Lib.h"

//--------------------------------------------------------------------------
class Simulation
{
public:
	Vehicle mVehicle;
	Path mPath;
	Path mTrackingPath;
	double mStepSize;
	double mTerminationTime;
	double mLookAheadValue;

	Simulation() :
		mVehicle(),
		mPath(),
		mTrackingPath(),
		mStepSize(0),
		mTerminationTime(0),
		mLookAheadValue(0)
	{};
	
	Simulation(const Vehicle& vVehicle, const Path& vPath, double vStepSize, double vTerminationTime, double vLookAheadValue)
	{
		//Set Vehicle Settings
		mVehicle.SetVehicleParameters(vVehicle.mLength, vVehicle.mSteeringDelay, vVehicle.mSteeringDumping, vVehicle.mSteeringBandwidth);
		mVehicle.SetVehicleState(vVehicle.mKinematicState);

		//Set Path to Follow
		mPath.SetDataSize(vPath.mData.GetSize());
		mPath.CopyData(vPath);
		
		//Set Simulation Parameters
		SetSimulationParameters(vStepSize, vTerminationTime, vLookAheadValue);
	}

	void SetSimulationParameters(double vStepSize, double vTerminationTime, double vLookAheadValue)
	{
		//Set Simulation Parameters
		mStepSize = vStepSize;
		mTerminationTime = vTerminationTime;
		mLookAheadValue = vLookAheadValue;
	}

	void Run()
	{
		double t, ref;
		double dx, dy, ld, alpha, k;
		int iRef;
		double delta_cmd;

		double dt = mStepSize;
		int N = static_cast<int>(mTerminationTime / dt);
		SmartPointer<double> tspan(N, 0);
		mTrackingPath.SetDataSize(N);
		mTrackingPath.mData.Reset(N);

		Path pathInGCS, pathInPCS;
		mPath.GetPath(pathInGCS, 0);
		mPath.GetPath(pathInPCS, 1);	//s coordinate is x coordinate in this object
		for (int i = 0; i < N; i++)
		{
			t = tspan[i];

			//Record the tracking path of the vehicle 
			SmartPointer<double> curStateGCS;
			mVehicle.GetState(curStateGCS);
			mTrackingPath.mData[i].x = curStateGCS[0];
			mTrackingPath.mData[i].y = curStateGCS[1];

			//Find the reference point via look ahead and transform to ECS
			ref = pathInPCS.mData[i].x + mLookAheadValue;
			iRef = FindNearestPointPCS(pathInPCS.mData, PathPoint(ref, 0));
			if (iRef <= i)
				iRef = i + 1;
			if (iRef >= N)
				iRef = N - 1;
			SmartPointer<PathPoint> refPosGCS(1, pathInGCS.mData[iRef]);
			SmartPointer<PathPoint> refPosECS(1);
			GCS2ECS(refPosGCS, PathPoint(curStateGCS[0], curStateGCS[1]), curStateGCS[4], refPosECS);

			//Calculate path curvature
			SmartPointer<double> curPosECS;
			mVehicle.GetState(curPosECS, 1);
			dx = refPosECS.mData[0].x - curPosECS[0];
			dy = refPosECS.mData[0].y - curPosECS[1];
			ld = sqrt(dx * dx + dy * dy);
			if (ld > 0)
			{
				alpha = atan2(dy, dx);
				k = 2 * sin(alpha) / ld;

				// Calculate the steering command delta
				delta_cmd = atan(2 * mVehicle.mLength * k);		// This is the steering command
			}
			mVehicle.ProgressVehicleMotion(t, dt, delta_cmd);

			dx = pathInGCS.mData[N-1].x - curStateGCS[0];
			dy = pathInGCS.mData[N-1].y - curStateGCS[1];
			if (sqrt(dx * dx + dy * dy) < 0.5)	// Vehicle reached final destination
				i = N;

			//std::cout << i << " " << iRef << " " << curStateGCS[0] << " " << curStateGCS[1] << " " << pathInGCS.mData[iRef].x << " " << pathInGCS.mData[iRef].y << std::endl;
		}
	}
};
