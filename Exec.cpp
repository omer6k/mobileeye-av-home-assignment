#include "Coordinate_System_Lib.h";
#include "Vehicle_Lib.h"
#include "Simulation_Lib.h"
#include <cstring>

//--------------------------------------------------------------------------
int main(int argc, char* argv[])
{
	if ((argc == 2) && ((static_cast<std::string>(argv[1]) == "?") || (static_cast<std::string>(argv[1]) == "help")))
	{
		std::cout << "Usage: FileName [x0, double] [y0, double] [psi0, double] [v0, double] [Path Type, 0=Circle, 1=Straight Line] [dt, double] [Termination Time, double]" << std::endl;
		std::cout << "Example (defaults): AV_PURE_PURSUIT_ASSIGNMENT.exe 0.1 0.1 0 1 0 0.1 60" << std::endl;
		return 1;
	}

	std::cout << "This is The Start of The Simulation..." << std::endl;
	int vScale = 10;
	double dt = 0.1;
	double TerminationTime = 60;
	int oPath = 0;
	double x0 = 0.1, y0 = 0.1, psi0 = 0.1, v0 = 1;
	if (argc > 1)
		x0 = std::stod(argv[1]);
	if (argc > 2)
		y0 = std::stod(argv[2]);
	if (argc > 3)
		psi0 = std::stod(argv[3]);
	if (argc > 4)
		v0 = std::stod(argv[4]);
	if (argc > 5)
		oPath = std::stoi(argv[5]);
	if (argc > 6)
		dt = std::stod(argv[6]);
	if (argc > 7)
		TerminationTime = std::stoi(argv[7]);

	std::cout << "Simulation Parameters: Total Time = " << TerminationTime << ", dt = " << dt << std::endl;
	std::cout << "Initial State: (x = " << x0 << ", y = " << y0 << ", psi = " << psi0 << ", v = " <<  v0 << ")" << std::endl;

	int N = int(TerminationTime / dt);
	Path p(1000);
	if (oPath == 0)
	{
		p.SetCircleTrajectory(PathPoint(15, -10), 15);
		std::cout << "Path is a " << "Circle with Center @(15,-10) & Radius=15" << std::endl;
	}
	else if (oPath == 1)
	{
		p.SetLineTrajectory(PathPoint(-30,-60), PathPoint(30, 85));
		std::cout << "Path is a " << "Straight Line (-30,-60) --> (30,85)" << std::endl;
		vScale = 10;
	}

	Vehicle v(2.728, 0.2, 0.707, 50);
	v.SetVehicleState(x0, y0, psi0, 0, v0);

	Simulation sim(v, p, dt, TerminationTime, 2 * dt);
	sim.Run();
	sim.mPath.Show(vScale);
	sim.mTrackingPath.Show(vScale);

	std::cout << "This is The End of The Simulation..." << std::endl;

	// Saving trajecories as image
	int w = 500, h = 500;
	cv::Mat img = cv::Mat(h, w, CV_8UC3, cv::Scalar(255, 255, 255));
	sim.mPath.Draw(img, h, w, vScale);
	sim.mTrackingPath.Draw(img, h, w, vScale);
	cv::imwrite("Path_VS_Track.png", img);
	std::cout << "Image Was Saved @Exec Path Directory" << std::endl;

	return 1;
}